//
//  ViewController.swift
//  GrimbergJason_Exercise05
//
//  Created by Jason Grimberg on 06/08/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textFirstNumber: UITextField!
    @IBOutlet weak var textSecondNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check labels
        label.text = "0"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Called when the user clicks on the view outside the text fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Function/Action to do all math equations
    @IBAction func doMath(sender: UIButton){
        // Make sure that the user has put a number in both text fields
        if (textFirstNumber.text != "" && ((textFirstNumber.text?.isInt) != nil)) &&
            (textSecondNumber.text != "" && ((textSecondNumber.text?.isInt) != nil)) {
                // Put strings into Ints
                let firstNumber:Int? = Int(textFirstNumber.text!)
                let secondNumber:Int? = Int(textSecondNumber.text!)
                var product: uint
        // Utilize the tag option with the buttons
        switch sender.tag {
        case 0:
            // Adds both numbers together and return the value
            product = uint(firstNumber! + secondNumber!)
            label.text = String(product)
        case 1:
            // Subtracts both numbers
            // Checks to see if the product will be a negative or not
            if(firstNumber! < secondNumber!){
                product = uint(secondNumber! - firstNumber!)
                label.text = String("-\(product)")
            }
            else{
                product = uint(firstNumber! - secondNumber!)
                label.text = String(product)
            }
        case 2:
            // Multiplies both numbers together and returns the value
            product = uint(firstNumber! * secondNumber!)
            label.text = String(product)
        case 3:
            // Divides both numbers together and returns the value
            if (firstNumber == 0 || secondNumber == 0){
                label.text = "0"
            } else {
                // Convert to double so you may have a decimal
                let result: Double = Double(firstNumber!) / Double(secondNumber!)
                label.text = String(result)
            }
        default:
            // Default to catch any error outside of scope
            print("An invalid button was pressed.")
            }
        } else {
            textFirstNumber.text = "0"
            textSecondNumber.text = "0"
            label.text = "0"
        }
    }
}
